/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 *
 * Sources:
 * https://www.geeksforgeeks.org/program-compare-two-fractions/
 * https://www.javatpoint.com/java-program-to-find-gcd-of-two-numbers
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {

      //Lfraction f1;
      //Lfraction f2;
      //f1 = new Lfraction (-2, 5);
//      f2 = new Lfraction (2, 5);
//      //System.out.println(f1);
//      Lfraction sum = f1.plus(f2);
//      //f2 = f1.clone();
//      Lfraction q1 = new Lfraction (1L, 2L);
//      int h1 = q1.hashCode();
//      Lfraction q2 = new Lfraction (0L, 2L);
//      int h2 = q2.hashCode();
      Lfraction f1 = new Lfraction(2, 3);
      Lfraction f2 = f1.pow(0);
      System.out.println(f2);
      // TODO!!! Your debugging tests here
   }

   // TODO!!! instance variables here

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {

      check_negative(this);
      common_divisor(this);

      if(b == 0)
      {
         throw new RuntimeException("Denominator is 0");
      }
      if(a == 0)
      {
         this.numerator = 0;
         this.denominator = 1;
      }
      else
      {
         numerator = a;
         denominator = b;
      }
      // TODO!!!
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return numerator; // TODO!!!
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return denominator; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + " / " + denominator;
      //return null; // TODO!!!
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

      Lfraction tocompare = (Lfraction) m;
      Lfraction.common_divisor((Lfraction) m);
      common_divisor(this);
      if (this.compareTo(tocompare) == 0) {
         return true;
      } else {
         return false; // TODO!!!
      }
   }


   /** Hashcode has to be the same for equal fractions and in general, different
    * for different fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return (int) ((int)denominator*denominator - ((int)numerator));

      //return 0; // TODO!!!
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      Lfraction sum  = new Lfraction(this.numerator * m.denominator +
              this.denominator * m.numerator,
              this.denominator * m.denominator);
      //return sum;
      Lfraction.common_divisor(sum);
      return sum;
      // TODO!!!
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      Lfraction product =  new Lfraction(this.getNumerator() * m.getNumerator(),
              this.getDenominator() * m.getDenominator()); // TODO!!!
      Lfraction.common_divisor(product);
      return  product;
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(this.denominator == 0)
      {
         throw new RuntimeException("Cannot divide by 0, your fraction is " + this);
      }
      Lfraction inverse = new Lfraction(this.getDenominator(), this.getNumerator());
      check_negative(inverse);
      return inverse;

      // TODO!!!
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {

      return new Lfraction(-1 * this.getNumerator(),this.getDenominator() ); // TODO!!!
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      Lfraction mopposite = m.opposite();
      Lfraction difference = this.plus(mopposite);
      // Lfraction difference = new Lfraction(this.plus());

      Lfraction.common_divisor(difference);
      return  difference;

   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if(m.denominator == 0)
      {
         throw new RuntimeException("Cannot divide by 0, your fraction is " + m);
      }
      Lfraction quotient =  new Lfraction(this.numerator * m.denominator,
              this.denominator * m.numerator); // TODO!!!
      Lfraction.common_divisor(quotient);

      check_negative(quotient);

      return quotient;
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {

      long comparison = this.getNumerator() * m.getDenominator()
              - this.getDenominator() * m.getNumerator();
      if(comparison < 0)
      {
         return -1;
      }
      else if(comparison == 0)
      {
         return 0;
      }
      else
      {
         return 1;
      }
      //return 0; // TODO!!!
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      Lfraction create = new Lfraction(1,1);
      create.numerator = this.numerator;
      create.denominator = this.denominator;
      return create; // TODO!!!
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {

      if(Math.abs(this.numerator) > Math.abs(this.denominator))
      {
         return this.numerator/this.denominator;
      }


      return 0L; // TODO!!!
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {

      long integer_part = this.integerPart();
      long reduced_part;
      if(integer_part > -1 ) {
         reduced_part = Math.abs(this.numerator) - Math.abs(integer_part) * this.denominator;
      }
      else
      {
         reduced_part = Math.abs(integer_part) * this.denominator - Math.abs(this.numerator);
      }

      return new Lfraction(reduced_part, this.denominator);
      // TODO!!!
   }

   /** Approximate value of the fraction.
    * @return real value of this fraction
    */
   public double toDouble() {

      double conversion;
      conversion = (double)this.numerator/(double)this.denominator;
      return conversion; // TODO!!!
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {

      double integer_part = Math.round(f*d);
      return new Lfraction((long) integer_part, d); // TODO!!!
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

      long a;
      long b;
      String illegalCharacters = "!@#$%&()',:;<=>?[]^_`{|}*+";
      int count_divisions=0;
      String division = "/";
      if(s.trim().isEmpty())
      {
         throw new RuntimeException(s + "Input string is empty");
      }
      String[] convert = s.trim().split("\\s+");
      if(convert.length > 3)
      {
         throw new RuntimeException("String must have form numerator / denominator; you have - " + s);
      }
      for(int i = 0; i < s.trim().length();i++) {
         char symbol = s.trim().charAt(i);
         if (illegalCharacters.contains(Character.toString(symbol))) {
            throw new RuntimeException(s + " expression has illegal symbol " + symbol);
         }
         if(division.contains(Character.toString(symbol)))
         {
            count_divisions++;
         }
      }
      if(count_divisions > 1)
      {
         throw new RuntimeException("String " + s + " cannot have more than 1 division, you have " + count_divisions);
      }
      a = Long.parseLong(convert[0]);
      b = Long.parseLong(convert[2]);
      return new Lfraction(a,b); // TODO!!!
   }
   private static void common_divisor(Lfraction m)
   {
      int GCD=1;
      long a = Math.abs(m.numerator);
      long b = Math.abs(m.denominator);
//    method to too slow, didn,t past the tests  while(a != b)
//      {
//         if (a > b)
//         {
//            a = a - b;
//         }
//         else
//         {
//            b = b - a;
//         }
//      }
      for(int i = 1;i <= a && i <= b;i++)
      {
         if(a%i==0 && b%i==0) {
            GCD = i;
         }
      }
      m.numerator = m.numerator/GCD;
      m.denominator = m.denominator/GCD;
   }
   private static void check_negative(Lfraction m)
   {
      if(m.numerator < 0 && m.denominator <0)
      {
         m.numerator = Math.abs(m.numerator);
         m.denominator = Math.abs(m.denominator);
      }
      else if(m.denominator < 0)
      {
         m.denominator = Math.abs(m.denominator);
         m.numerator = m.numerator * -1;

      }
   }
   public Lfraction pow(int power)
   {
      if(power == 0)
      {
         Lfraction result = new Lfraction(1,1);
         return result;
      }
      else if(power < 0)
      {
         Lfraction result= new Lfraction(this.numerator,this.denominator);
         //power = -1 * power;
         return result.pow(-1*power).inverse();
      }
      else if(power > 1)
      {
         Lfraction result= new Lfraction(this.numerator,this.denominator);
         return result.times(result.pow(power -1 ));
      }
      else {
         Lfraction result = new Lfraction(this.numerator, this.denominator);
         return result;
      }
   }

}
